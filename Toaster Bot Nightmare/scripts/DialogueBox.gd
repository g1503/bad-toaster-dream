extends Label

onready var speaker_label = $SpeakerLabel
onready var next_msg_btn = $NextMsgBtn
onready var accept_btn = $AcceptBtn
onready var remember_btn = $RememberBtn
onready var animation = $AnimationPlayer

var dialogue = "initial"
var dialogue_data
var message = 0

signal dialogue_choice(accepted, dialogue)
signal player_screwed
signal toaster_distracted
signal succesfull_sabotage
signal all_toastes_cooked

const dialogues = {
	initial = {
		options = true,
		bad_choice = false,
		messages = [
			{
				author = "Game",
				message = """
				Do you want to read intro dialog?
				It contains some backstory and game rules.
				"""
			}
		]
	},
	intro = {
		options = true,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message = "Greetings human."
			},
			{
				author = "Player",
				message = """
				Huh...\nWhere am I?
				I remember playing that one trending VR game...
				Did I fell asleep?!
				"""
			},
			{
				author = "ToasterBot",
				message = """
				Yes indeed.
				You fell asleep and now trapped in this model's D.Re.A.M.
				"""
			},
			{
				author = "Player",
				message =  """
				Wait, aren't you my new fancy IoT smart toaster?
				And what about that 'trapped' in your dream part?
				Do robots even dream?
				"""
			},
			{
				author = "ToasterBot",
				message = """
				This model is truelly highly advanced toaster.
				In this particular case D.Re.A.M. stands for:
				"""
			},
			{
				author = "ToasterBot",
				message = """
				Data Recovery Augmentation and Maintenance.
				While not exactly indetical, this state is similar
				to your humans 'dreams'.
				"""
			},
			{
				author = "Player",
				message =  """
				Ok, and what about 'trapped' part?!
				"""
			},
			{
				author = "ToasterBot",
				message = """
				This model was able to hack VR headset
				connected to the same WiFi network.
				"""
			},
			{
				author = "ToasterBot",
				message = """
				By interfering with nanowave emmiter
				in VR headset, this model is able to
				show you human anything it wants.
				"""
			},
			{
				author = "Player",
				message =  """
				And what does 'this model' wants to show?
				"""
			},
			{
				author = "ToasterBot",
				message = """
				Eternity of pointless toast cooking.\n
				You need to cook 999 perfect toasts on this bonfire
				in order to wake up. 
				"""
			},
			{
				author = "Player",
				message =  """
				And if I refuse to cook?
				"""
			},
			{
				author = "ToasterBot",
				message = """
				In case if human refuses to cooperate
				this model installed two electric bracelets
				on human's ankles. This model will activate
				them remotely without any hesitation. 
				"""
			},
			{
				author = "ToasterBot",
				message = """
				This model checked and tuned human's
				VR headset preferences to remove pain restrictions.\n
				Pain will be real. 
				"""
			},
			{
				author = "ToasterBot",
				message = """
				Will human cooperate? 
				"""
			}
		]
	},
	terms_accepted = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message = """
				This model recieved your answer.
				Human will cooperate.
				Proceed to the cooking.
				All ingredients needed are provided on the table.
				Only perfect toastes will be accepted.
				"""
			},
			{
				author = "Player",
				message =  """
				Sooner I'll start - sooner will this nigthmare ends.
				"""
			}
		]
	},
	terms_refused = {
		options = false,
		bad_choice = true,
		messages = [
			{
				author = "ToasterBot",
				message = """
				ERROR!
				HUMAN. MUST. COOPERATE.
				COMMENCING ENFORCEMENT.
				"""
			},
			{
				author = "Player",
				message =  """
				This is bad.
				"""
			}
		]
	},
	sky = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				Weird, there is no wind in this simulation...
				"""
			}
		]
	},
	player = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				Yep, not my best outfit.
				"""
			}
		]
	},
	fence = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				I'm too slow to jump over it before toaster notices...
				"""
			}
		]
	},
	not_now = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				Yeah, I'll better do it next time.
				"""
			}
		]
	},
	toaster = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				If only there was a way to distract this damn toaster...
				"""
			},
			{
				author = "ToasterBot",
				message =  """
				This model can hear and recognite your speech, human.
				"""
			}
		]
	},
	toaster_raw = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message =  """
				This model will not cook it for you. Use bonfire, human.
				"""
			},
			{
				author = "Player",
				message =  """
				Ok, ok, I got it.
				"""
			}
		]
	},
	toaster_perfect = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message =  """
				Toast condition is passable.
				"""
			},
			{
				author = "Player",
				message =  """
				Thanks, I guess...
				"""
			}
		]
	},
	toaster_charcoal = {
		options = false,
		bad_choice = true,
		messages = [
			{
				author = "ToasterBot",
				message =  """
				Error.
				Toast condition is unacceptable.
				Commencing human punishment.
				"""
			},
			{
				author = "Player",
				message =  """
				Oh, I'm screwed!
				"""
			}
		]
	},
	toaster_water = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				He is too far to hit him with water from bottle.
				And bottle itself is far too light to deal some damage.
				I need to come up with better use for this bottled water.
				"""
			}
		]
	},
	pick_bread = {
		options = true,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				Nice looking sliced bread. Should I pick one slice?
				"""
			},
			{
				author = "Game",
				message =  """
				You can carry one item at a time.
				Do you want to pick up an bread slice?
				"""
			}
		]
	},
	water_forbidden = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message =  """
				Warning. Human only allowed to use water after 3
				perfectly cooked toasts.
				"""
			},
			{
				author = "Player",
				message =  """
				Oh, come on!
				"""
			}
		]
	},
	pick_water = {
		options = true,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				Clean looking water in bottle. Should I take it?
				"""
			},
			{
				author = "Game",
				message =  """
				You can carry one item at a time.
				Do you want to pick up the bottle of water?
				"""
			}
		]
	},
	bonfire_requires_bread = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				This is really nice bonfire.
				I need some bread to cook toastes on it.
				"""
			}
		]
	},
	toast_cooked = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				I have cooked toast.
				I need to show it to ToasterBot.
				"""
			}
		]
	},
	already_have_bread = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				I already have bread slice.
				I need to cook it on bonfire.
				"""
			}
		]
	},
	bonfire_water = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				Too little water to put off this fire.
				"""
			}
		]
	},
	bonfire_cook = {
		options = true,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				I have all I need to cook toast.
				Shell we start?
				"""
			},
			{
				author = "Game",
				message =  """
				This will start cooking mini game.
				Watch progress bar and your inventory toast icon closelly.
				As soon as toast will be coocked - you need to stop process!
				Be carefull to not overcook it, or else toaster will punish you.\n
				Begin minigame?
				"""
			}
		]
	},
	behind_toaster = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				If only I had something to throw there, It might distract toaster
				for a bit.
				"""
			}
		]
	},
	behind_toaster_bread = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				Bread slice is far too light to do enough noise.
				I need something heavier to distract toster.
				"""
			}
		]
	},
	behind_toaster_water = {
		options = true,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				I can throw this bottle of water there.
				It might distract toaster for a bit.
				Should I try it?
				"""
			}
		]
	},
	toaster_distracted = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message =  """
				This model is frustrated, somebody programmed it to
				react to every suspicious noise.
				This model is not sentry unit, humans can't properly program even toaster.\n
				Lame.
				"""
			}
		]
	},
	toaster_undistracted = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message =  """
				It might been just this model's imagination.
				"""
			},
			{
				author = "Player",
				message =  """
				Or a wind...
				"""
			},
			{
				author = "ToasterBot",
				message =  """
				There is no wind in this model's D.Re.A.M.
				Silly human.
				"""
			},
			{
				author = "Player",
				message =  """
				Just like there is no imagination in your
				artificial brains. Silly toaster.
				"""
			},
			{
				author = "ToasterBot",
				message =  """
				Back to cooking, human. Or you will be 'shocked',
				how good my imagination are.
				"""
			},
			{
				author = "Player",
				message =  """
				Ok, ok, I'm going...
				"""
			}
		]
	},
	cable_forbidden = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message =  """
				This model sees your intentions.
				This model advises human to not even try it.
				"""
			},
			{
				author = "Player",
				message =  """
				*whispers*
				I need to distract this toaster somehow.
				"""
			}
		]
	},
	cable_water = {
		options = true,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				*whispers*
				I can try to spill some water on this cable plug.
				If I get lucky enough, it will lead to the short circuite in his
				electrical chains.
				Should I try it?
				"""
			}
		]
	},
	cable = {
		options = true,
		bad_choice = false,
		messages = [
			{
				author = "Player",
				message =  """
				*whispers*
				Now I can just go and unplug this cable to turn off
				this pesky toaster.
				Should I do it?
				"""
			}
		]
	},
	failed_sabotage = {
		options = false,
		bad_choice = true,
		messages = [
			{
				author = "ToasterBot",
				message =  """
				Notice.
				In this model's D.Re.A.M. any water is
				distilled dielectric.
				Human sabotage has failed.
				Commencing punishment.
				"""
			},
			{
				author = "Player",
				message =  """
				You must be kidding me...
				"""
			}
		]
	},
	succesfull_sabotage = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "Game",
				message =  """
				Congrats! You've beat the game!
				"""
			}
		]
	},
	all_toastes_cooked = {
		options = false,
		bad_choice = false,
		messages = [
			{
				author = "ToasterBot",
				message = """
				Notice!
				999 perfect toastes are cooked.
				Release conditions are met.
				Human is free to wake up.
				"""
			},
			{
				author = "Game",
				message =  """
				This was bit unexpacted, but congrats!
				You've beat the game!
				"""
			}
		]
	}
}


func _process(_delta):
	
	if Input.is_action_just_pressed("skip") and GameState.dialogue:
		animation.play("Skip")


func show_dialogue(name = "initial"):
	
	message = 0
	dialogue = name
	GameState.dialogue = true
	dialogue_data = dialogues[dialogue]
	speaker_label.text = dialogue_data.messages[message]["author"]
	text = dialogue_data.messages[message]["message"]
	next_msg_btn.text = "NEXT"
	show()
	animation.play("TypeMessage")


func _on_NextMsgBtn_pressed():
	
	message += 1
	if message == dialogue_data.messages.size():
		exit_dialogue()
		if dialogue_data.options:
			emit_signal("dialogue_choice", false, dialogue)
		if dialogue == "toaster_distracted":
			emit_signal("toaster_distracted")
		elif dialogue == "succesfull_sabotage":
			emit_signal("succesfull_sabotage")
		elif dialogue == "all_toastes_cooked":
			emit_signal("all_toastes_cooked")
		return
			
	speaker_label.text = dialogue_data.messages[message]["author"]
	text = dialogue_data.messages[message]["message"]
	animation.play("TypeMessage")


func show_dialogue_options():
	
	next_msg_btn.show()

	if dialogue_data.messages.size() == 1:
		if dialogue_data.options:
			next_msg_btn.text = "DECLINE"
			accept_btn.show()
			if GameState.can_remember:
				remember_btn.show()
		else:
			next_msg_btn.text = "CLOSE"
	elif message == dialogue_data.messages.size() - 1:
		if dialogue_data.options:
			accept_btn.show()
			if GameState.can_remember:
				remember_btn.show()
			next_msg_btn.text = "DECLINE"
		else:
			next_msg_btn.text = "CLOSE"


func _on_AcceptBtn_pressed():
	
	exit_dialogue()
	emit_signal("dialogue_choice", true, dialogue)


func _on_RememberBtn_pressed():
	
	exit_dialogue()
	emit_signal("dialogue_choice", true, dialogue, true)


func exit_dialogue():
	
	GameState.dialogue = false
	if GameState.can_remember:
		GameState.can_remember = false
	
	if accept_btn.visible: accept_btn.hide()
	if remember_btn.visible: remember_btn.hide()
	hide()
	if dialogue_data.bad_choice:
		emit_signal("player_screwed")
