extends Node2D

var started = false

onready var start_btn = $UI/Control/StartBtn
onready var dialogue_box = $UI/Control/DialogueBox
onready var player_sprite = $Player
onready var bot_sprite = $ToasterBot
onready var cable_sprite = $Cable
onready var bot_timer_label = $ToasterBot/TimerLabel
onready var bot_distract_timer = $ToasterBot/DistractTimer
onready var cooking_bar = $UI/Control/CookingBar
onready var bipod_sprite = $BipodSprite
onready var water_btn = $UI/Control/Water
onready var inventory = $UI/Control/Inventory
onready var inventory_label = $UI/Control/Inventory/InventoryLabel
onready var inventory_water = $UI/Control/Inventory/Water
onready var inventory_toast = $UI/Control/Inventory/Toast
onready var score = $UI/Control/ScoreLabel
onready var score_icon = $UI/Control/ScoreIcon

enum PlayerInventoryStates {Empty, RawToast, PerfectToast, Charcoal, Water}
var player_inventory_state = PlayerInventoryStates.Empty

var perfect_toastes_score = 0
var max_perfect = 85
var min_perfect = 65
var max_bonfire_power = 80
var min_bonfire_power = 10
var bonfire_power = 0

var cooking = false

var toaster_distracted = false

func _ready():
	
	cooking_bar.max_value = 100
	cooking_bar.min_value = 0


func _process(delta):
	
	if toaster_distracted:
		bot_timer_label.text = "%s"%int(bot_distract_timer.time_left)
		
	
	if cooking:
		if bonfire_power < max_bonfire_power:
			bonfire_power = min_bonfire_power + 5 * delta
		else:
			bonfire_power = max_bonfire_power
		
		cooking_bar.value += max_bonfire_power * delta
		
		if cooking_bar.value >= 100:
			cooking_game_stop()
		elif cooking_bar.value >= max_perfect:
			player_inventory_state = 3
			update_inventory()
		elif cooking_bar.value >= min_perfect:
			player_inventory_state = 2
			update_inventory()


func update_inventory():
	
	match player_inventory_state:
		0:
			inventory_label.text = "Empty"
			inventory_water.hide()
			inventory_toast.hide()
		1:
			inventory_label.text = "Raw Toast"
			inventory_water.hide()
			inventory_toast.show()
			inventory_toast.play("raw")
			if !water_btn.visible and !GameState.water_used: water_btn.show()
		2:
			inventory_label.text = "Perfect Toast"
			inventory_water.hide()
			inventory_toast.show()
			inventory_toast.play("perfect")
		3:
			inventory_label.text = "Charcoal"
			inventory_water.hide()
			inventory_toast.show()
			inventory_toast.play("charcoal")
		4:
			inventory_label.text = "Water"
			inventory_water.show()
			inventory_toast.hide()
			water_btn.hide()


func _on_StartBtn_pressed():
	
	if !started:
		started = true
		start_btn.hide()
		dialogue_box.show_dialogue()
		start_btn.text = "RESTART"
	else:
		restart()


func cooking_game_start():
	
	get_tree().call_group("CookingUI", "show")
	inventory.rect_position = Vector2(407,244);
	cooking_bar.value = 0
	cooking = true
	bipod_sprite.play("cook")


func cooking_game_stop():
	
	get_tree().call_group("CookingUI", "hide")
	inventory.rect_position = Vector2(872,128);
	cooking = false
	bipod_sprite.play("idle")


func _on_DialogueBox_dialogue_choice(accepted, dialogue, remember = false):
	
	match dialogue:
		"initial":
			if accepted:
				dialogue_box.show_dialogue("intro")
			else:
				print("Impatient one!")
				inventory.show()
				score.show()
				score_icon.show()
			
		"intro":
			if accepted:
				dialogue_box.show_dialogue("terms_accepted")
				inventory.show()
				score.show()
				score_icon.show()
			else:
				dialogue_box.show_dialogue("terms_refused")
		"pick_bread":
			if accepted:
				player_inventory_state = PlayerInventoryStates.RawToast
				update_inventory()
				if remember:
					GameState.toastes_explained = true
			else:
				dialogue_box.show_dialogue("not_now")
		"pick_water":
			if accepted:
				player_inventory_state = PlayerInventoryStates.Water
				update_inventory()
				if remember:
					GameState.water_explained = true
			else:
				dialogue_box.show_dialogue("not_now")
		"bonfire_cook":
			if accepted:
				cooking_game_start()
				if remember:
					GameState.cooking_explained = true
			else:
				dialogue_box.show_dialogue("not_now")
		"behind_toaster_water":
			if accepted:
				bot_sprite.flip_h = true
				toaster_distracted = true
				player_inventory_state = PlayerInventoryStates.Empty
				GameState.water_used = true
				update_inventory()
				dialogue_box.show_dialogue("toaster_distracted")
			else:
				dialogue_box.show_dialogue("not_now")
		"cable_water":
			if accepted:
				dialogue_box.show_dialogue("failed_sabotage")
				GameState.water_used = true
			else:
				dialogue_box.show_dialogue("not_now")
		"cable":
			if accepted:
				cable_sprite.frame = 1
				bot_sprite.playing = false
				bot_timer_label.hide()
				bot_distract_timer.stop()
				dialogue_box.show_dialogue("succesfull_sabotage")
			else:
				dialogue_box.show_dialogue("not_now")


func _on_DialogueBox_player_screwed():
	
	player_sprite.animation = "shocked"
	bot_sprite.animation = "attack"
	start_btn.show()


func restart():

# warning-ignore:return_value_discarded
	get_tree().reload_current_scene()


func _on_Bread_pressed():
	
	if player_inventory_state == 0 or player_inventory_state == 4:
		if GameState.toastes_explained:
			player_inventory_state = PlayerInventoryStates.RawToast
			update_inventory()
		else:
			dialogue_box.show_dialogue("pick_bread")
			GameState.can_remember = true
	elif player_inventory_state == 1:
		dialogue_box.show_dialogue("already_have_bread")
	else:
		dialogue_box.show_dialogue("toast_cooked")


func _on_Water_pressed():
	
	if !cooking and !GameState.dialogue and started:
		if player_inventory_state <= 1:
			if perfect_toastes_score < 3:
				dialogue_box.show_dialogue("water_forbidden")
			else:
				if GameState.water_explained:
					player_inventory_state = PlayerInventoryStates.Water
					update_inventory()
				else:
					dialogue_box.show_dialogue("pick_water")
					GameState.can_remember = true
		else:
			dialogue_box.show_dialogue("toast_cooked")


func _on_Cable_pressed():
	
	if !cooking and !GameState.dialogue and started:
		if player_inventory_state == 4:
			dialogue_box.show_dialogue("cable_water")
		elif toaster_distracted:
			dialogue_box.show_dialogue("cable")
		else:
			dialogue_box.show_dialogue("cable_forbidden")


func _on_Player_pressed():
	
	if !cooking and !GameState.dialogue and started:
		dialogue_box.show_dialogue("player")


func _on_Toaster_pressed():
	
	if !cooking and !GameState.dialogue and started:
		match player_inventory_state:
			0:
				dialogue_box.show_dialogue("toaster")
			1:
				dialogue_box.show_dialogue("toaster_raw")
			2: 
				if !GameState.perfect_toast_explained:
					dialogue_box.show_dialogue("toaster_perfect")
					GameState.perfect_toast_explained = true
				perfect_toastes_score += 1
				score.text = "%s:"%perfect_toastes_score
				player_inventory_state = PlayerInventoryStates.Empty
				update_inventory()
				if perfect_toastes_score >= 999:
					dialogue_box.show_dialogue("all_toastes_cooked")
			3:
				dialogue_box.show_dialogue("toaster_charcoal")
			4:
				dialogue_box.show_dialogue("toaster_water")


func _on_Bonfire_pressed():
	
	if !cooking and !GameState.dialogue and started:
		match player_inventory_state:
			0:
				dialogue_box.show_dialogue("bonfire_requires_bread")
			1:
				if GameState.cooking_explained:
					cooking_game_start()
				else:
					dialogue_box.show_dialogue("bonfire_cook")
					GameState.can_remember = true
			2: 
				dialogue_box.show_dialogue("toast_cooked")
			3:
				dialogue_box.show_dialogue("toast_cooked")
			4:
				dialogue_box.show_dialogue("bonfire_water")


func _on_BehindToaster_pressed():
	
	if !cooking and !GameState.dialogue and started:
		match player_inventory_state:
			0:
				dialogue_box.show_dialogue("behind_toaster")
			1:
				dialogue_box.show_dialogue("behind_toaster_bread")
			2: 
				dialogue_box.show_dialogue("toast_cooked")
			3:
				dialogue_box.show_dialogue("toast_cooked")
			4:
				dialogue_box.show_dialogue("behind_toaster_water")


func _on_Sky_pressed():
	
	if !cooking and !GameState.dialogue and started:
		dialogue_box.show_dialogue("sky")


func _on_Fence_pressed():
	
	if !cooking and !GameState.dialogue and started:
		dialogue_box.show_dialogue("fence")


func _on_AbortCooking_pressed():
	
	cooking_game_stop()


func _on_DialogueBox_toaster_distracted():
	
	bot_distract_timer.start()
	bot_timer_label.show()


func _on_DistractTimer_timeout():
	
	bot_timer_label.hide()
	bot_sprite.flip_h = false
	toaster_distracted = false
	dialogue_box.show_dialogue("toaster_undistracted")


func _on_DialogueBox_succesfull_sabotage():
	
	start_btn.show()


func _on_DialogueBox_all_toastes_cooked():
	
	start_btn.show()
